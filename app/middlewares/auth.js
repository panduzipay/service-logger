const basicAuthModule = require('basic-auth');
const moment = require('moment');
const Moment = require('moment');
const get = require('lodash/get');
const {
  AESCBCDecrypt,
} = require('../libs');

const basicAuth = () => (req, res, next) => {
  try {
    const timestamp = get(req, 'headers.timestamp');
    if (!timestamp) throw new Error();

    // const requestId = get(req, 'headers.requestid');
    // if (requestId) process.requestId = requestId;

    const getTimeStamp = timestamp.slice(0, 13);
    const convertTimeStamp = moment(new Date(Number(getTimeStamp))).utcOffset('+07:00').format();
    const dateNow = moment().utcOffset('+07:00').format();
    const getDuration = Math.abs(Number(moment.duration((new Moment(convertTimeStamp))
      .diff(new Moment(dateNow))).asSeconds()));
    if (getDuration >= 1000) {
      throw Object.assign(new Error(), {
        data: {
          dateNow,
          getTimeStamp,
          getDuration,
        },
      });
    }
    const user = basicAuthModule(req);
    if (!user) throw Object.assign(new Error('user'), { data: req });

    const nameBasic = AESCBCDecrypt(timestamp, get(user, 'name'));
    const passBasic = AESCBCDecrypt(timestamp, get(user, 'pass'));

    if (nameBasic !== process.env.AUTH_NAME || passBasic !== process.env.AUTH_PASS) {
      throw Object.assign(new Error('credentials'), { data: nameBasic, passBasic });
    } else {
      next();
    }
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(get(error, 'data'));
    res.status(401);
    if (error.message === 'user') {
      res.set('WWW-Authenticate', 'Basic realm=Authorization Required');
    }
    res.json({
      status: 401,
      success: false,
      message: 'UNAUTHORIZED',
      data: null,
    });
  }
};

module.exports = {
  basicAuth,
};
