const head = require('lodash/head');
const upperCase = require('lodash/upperCase');
const get = require('lodash/get');

const {
  wrap,
  logInfo,
} = require('../../helpers');
// const {
//   basicAuth,
// } = require('../../middlewares');

const loggerInfoApi = async (req) => {
  req.checkBody('data').not().isEmpty().withMessage('data is required');
  req.checkBody('message').not().isEmpty().withMessage('message is required')
    .customSanitizer(value => String(value));
  req.checkBody('method').not().isEmpty().withMessage('method is required')
    .customSanitizer(value => upperCase(String(value)));
  req.checkBody('mep').not().isEmpty().withMessage('mep is required')
    .customSanitizer(value => upperCase(String(value)));
  req.checkBody('toService').not().isEmpty().withMessage('toService is required')
    .customSanitizer(value => String(value));
  req.checkBody('statusCode').not().isEmpty().withMessage('statusCode is required')
    .customSanitizer(value => String(value));
  req.checkBody('requestId').not().isEmpty().withMessage('requestId is required')
    .customSanitizer(value => String(value));
  req.checkBody('fromService').not().isEmpty().withMessage('fromService is required')
    .customSanitizer(value => String(value));
  req.checkBody('label').not().isEmpty().withMessage('label is required')
    .customSanitizer(value => String(value));

  const errors = req.validationErrors();
  if (errors) throw Object.assign(new Error('Validation Errors'), { data: head(errors) });

  if (get(req, 'body.statusCode') === 'no statusCode') req.body.statusCode = '200';
  await logInfo({
    ...req.body,
  });

  const result = {
    dataLog: req.body,
  };

  return result;
};

module.exports = (router) => {
  router.post('/', wrap(loggerInfoApi));
};
