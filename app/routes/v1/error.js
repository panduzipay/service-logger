const head = require('lodash/head');
const get = require('lodash/get');
const upperCase = require('lodash/upperCase');
const fastSafeStringify = require('fast-safe-stringify');

const {
  wrap,
  logError,
} = require('../../helpers');
// const {
//   basicAuth,
// } = require('../../middlewares');
const {
  sendRequest,
} = require('../../request');

const loggerErrorApi = async (req) => {
  req.checkBody('data').not().isEmpty().withMessage('data is required');
  req.checkBody('message').not().isEmpty().withMessage('message is required')
    .customSanitizer(value => String(value));
  req.checkBody('method').not().isEmpty().withMessage('method is required')
    .customSanitizer(value => upperCase(String(value)));
  req.checkBody('mep').not().isEmpty().withMessage('mep is required')
    .customSanitizer(value => upperCase(String(value)));
  req.checkBody('toService').not().isEmpty().withMessage('toService is required')
    .customSanitizer(value => String(value));
  req.checkBody('statusCode').not().isEmpty().withMessage('statusCode is required')
    .customSanitizer(value => String(value));
  req.checkBody('requestId').not().isEmpty().withMessage('requestId is required')
    .customSanitizer(value => String(value));
  req.checkBody('fromService').not().isEmpty().withMessage('fromService is required')
    .customSanitizer(value => String(value));
  req.checkBody('label').not().isEmpty().withMessage('label is required')
    .customSanitizer(value => String(value));

  const errors = req.validationErrors();
  if (errors) throw Object.assign(new Error('Validation Errors'), { data: head(errors) });

  if (get(req, 'body.statusCode') === 'no statusCode') req.body.statusCode = '200';
  await logError({
    ...req.body,
  });

  const result = {
    dataLog: req.body,
  };

  const resultValue = `\`\`\`${fastSafeStringify(get(req, 'body.data'), null, 2)}\`\`\``;

  sendRequest({
    url: process.env.URI_SLACK_WEB_HOOK,
    method: 'POST',
    data: {
      attachments: [
        {
          fallback: `Error from ${get(req, 'body.fromService')} to ${get(req, 'body.toService')} [${get(req, 'body.requestId')}]: <${process.env.KIBANA_URI}/app/discover#/?_g=(filters:!(),refreshInterval:(pause:!t,value:0),time:(from:now-15d,to:now))&_a=(columns:!(toService,statusCode,requestId,message,mep,level,labelService,data),filters:!(),index:'${process.env.KIBANA_INDEX_ID}',interval:auto,query:(language:kuery,query:${get(req, 'body.requestId')}),sort:!(!('@timestamp',desc)))|Detail Error>`,
          pretext: `Error from ${get(req, 'body.fromService')} to ${get(req, 'body.toService')} [${get(req, 'body.requestId')}]: <${process.env.KIBANA_URI}/app/discover#/?_g=(filters:!(),refreshInterval:(pause:!t,value:0),time:(from:now-15d,to:now))&_a=(columns:!(toService,statusCode,requestId,message,mep,level,labelService,data),filters:!(),index:'${process.env.KIBANA_INDEX_ID}',interval:auto,query:(language:kuery,query:${get(req, 'body.requestId')}),sort:!(!('@timestamp',desc)))|Detail Error>`,
          
          color: 'danger',
          fields: [
            {
              title: `${get(req, 'body.label')}`,
              value: `${resultValue}`,
              short: false,
            },
          ],
        },
      ],
    },
  }).catch(() => ({}));
  return result;
};

module.exports = (router) => {
  router.post('/', wrap(loggerErrorApi));
};
