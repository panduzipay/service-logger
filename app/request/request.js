const axios = require('axios');
const get = require('lodash/get');
const qs = require('qs');

const sendRequest = async ({
  url,
  method,
  data,
  params,
  optHeaders,
}) => {
  let contentType = 'application/json';
  let dataParse = data;
  if (optHeaders) {
    contentType = get(optHeaders, 'contentType');
    if (contentType !== 'application/json') {
      dataParse = qs.stringify(data);
    }
  }
  const headers = {
    'Content-Type': contentType,
    ...optHeaders,
  };

  delete headers.contentType;

  const requestConfig = {
    method,
    url,
    headers,
    data: dataParse,
    params,
  };

  if (!data) delete requestConfig.data;
  if (!params) delete requestConfig.params;

  const req = await axios(requestConfig);

  const response = get(req, 'data');

  return response;
};

module.exports = {
  sendRequest,
};
